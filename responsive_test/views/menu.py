from django.shortcuts import redirect
from django.views.generic import TemplateView

class ProtectedView(TemplateView):
    def dispatch(self, *args, **kwargs):
        return super(ProtectedView, self).dispatch(*args, **kwargs)

def index(request):
    return redirect('menu:index')
