
# ヘッダのロゴ名
def logo_name(request):
    logo_name = None
    if not logo_name:  # Agent に未設定時も含む
        logo_name = 'test logo'
    return {'logo_name': logo_name}

# メニュー一覧
def menu_list(request):
    menus = []
    menus.append({'name': 'HOME', 'url': "#"})
    menus.append({'name': 'PRODUCT', 'url': "#"})
    menus.append({'name': 'WORKS', 'url': "#"})
    menus.append({'name': 'COMPANY', 'url': "#"})
    menus.append({'name': 'TEST', 'url': "#"})

    return {'menus': menus}